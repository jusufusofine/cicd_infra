

resource "aws_instance" "app" {
  ami                    = var.AMIS.Ubuntu_Server_18_04_LTS_SSD
  instance_type          = "t2.nano"
  availability_zone      = var.ZONE1
  key_name               = aws_key_pair.infra_key.key_name
  vpc_security_group_ids = [aws_security_group.app_sg.id]
  tags = {
    "name" = "app"
  }

}

output "public_ip_app" {
  value = aws_instance.app.public_ip
}

resource "aws_security_group" "app_sg" {
  name        = "app_sg"
  description = "app_sg"

  ingress {
    description = "public web app"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "personal use"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [format("%s/%s", data.external.whatismyip.result["internet_ip"], 32)]
  }

   ingress {
    description = "usage from jenkins"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.jenkins_sg.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

data "template_file" "inventory" {
  template = file("inventory.tmpl")
  vars = {
    "group" = "[appsrvgrp]",
    "ip" = aws_instance.app.private_ip
  }
}

resource "local_file" "inventory" {
    filename = "./inventory.inv"
    content     = data.template_file.inventory.rendered
}
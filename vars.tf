
variable "REGION" {
  default = "us-east-1"
}

variable "ZONE1" {
  default = "us-east-1a"
}

variable "AMIS" {

  type = object({
    Amazon_Linux_2_AMI_HVM_Kernel_5_10_SSD = string,
    Ubuntu_Server_18_04_LTS_SSD            = string,
    old_ami                                = string
  })

  default = {
    Amazon_Linux_2_AMI_HVM_Kernel_5_10_SSD = "ami-0022f774911c1d690",
    Ubuntu_Server_18_04_LTS_SSD            = "ami-005de95e8ff495156",
    old_ami                                = "ami-06eecef118bbf9259"
  }
}

variable "USERS" {
  type = object({
    ubuntu   = string,
    ec2-user = string
  })

  default = {
    ubuntu   = "ubuntu",
    ec2-user = "ec2-user"
  }
}

variable "KEYS" {
  type = object({
    public_key  = string,
    private_key = string
  })

  default = {
    public_key  = "infra_key.pub"
    private_key = "infra_key"
  }

}



resource "aws_key_pair" "infra_key" {
  key_name   = "infra_key"
  public_key = file(var.KEYS.public_key)
}

resource "aws_instance" "jenkins" {
  ami                    = var.AMIS.Ubuntu_Server_18_04_LTS_SSD
  instance_type          = "t2.small"
  availability_zone      = var.ZONE1
  key_name               = aws_key_pair.infra_key.key_name
  vpc_security_group_ids = [aws_security_group.jenkins_sg.id]
  tags = {
    "name" = "jenkins"
  }

  provisioner "file" {
    source      = "provisioning_scripts/jenkins-setup.sh"
    destination = "/tmp/jenkins-setup.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod u+x /tmp/jenkins-setup.sh",
      "sudo /tmp/jenkins-setup.sh"
    ]
  }

  connection {
    user        = var.USERS.ubuntu
    private_key = file(var.KEYS.private_key)
    host        = self.public_ip
  }
}

output "public_ip_jenkins" {
  value = aws_instance.jenkins.public_ip
}

resource "aws_instance" "nexus" {
  ami                    = var.AMIS.Amazon_Linux_2_AMI_HVM_Kernel_5_10_SSD
  instance_type          = "t2.small"
  availability_zone      = var.ZONE1
  key_name               = aws_key_pair.infra_key.key_name
  vpc_security_group_ids = [aws_security_group.nexus_sg.id]
  tags = {
    "name" = "nexus"
  }

  provisioner "file" {
    source      = "provisioning_scripts/nexus-setup.sh"
    destination = "/tmp/nexus-setup.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod u+x /tmp/nexus-setup.sh",
      "sudo /tmp/nexus-setup.sh"
    ]
  }

  connection {
    user        = var.USERS.ec2-user
    private_key = file(var.KEYS.private_key)
    host        = self.public_ip
  }
}

output "public_ip_nexus" {
  value = aws_instance.nexus.public_ip
}

resource "aws_instance" "sonarqube" {
  ami                    = var.AMIS.Ubuntu_Server_18_04_LTS_SSD
  instance_type          = "t2.small"
  availability_zone      = var.ZONE1
  key_name               = aws_key_pair.infra_key.key_name
  vpc_security_group_ids = [aws_security_group.sonar_sg.id]
  tags = {
    "name" = "sonarqubee"
  }

  provisioner "file" {
    source      = "provisioning_scripts/sonar-setup.sh"
    destination = "/tmp/sonar-setup.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod u+x /tmp/sonar-setup.sh",
      "sudo /tmp/sonar-setup.sh",
      "reboot"
    ]
  }


  connection {
    user        = var.USERS.ubuntu
    private_key = file(var.KEYS.private_key)
    host        = self.public_ip
  }
}

output "public_ip_sonarqube" {
  value = aws_instance.sonarqube.public_ip
}

data "external" "whatismyip" {
  program = ["/bin/bash", "what_is_my_ip.sh"]
}


resource "aws_security_group" "jenkins_sg" {
  name        = "jenkins_sg"
  description = "jenkins_sg"

  ingress {
    description = "public jenkins dashboard (gitlab hooks)"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "personal use for jenkins"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [format("%s/%s", data.external.whatismyip.result["internet_ip"], 32)]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_security_group" "nexus_sg" {
  name        = "nexus_sg"
  description = "nexus_sg"

  ingress {
    description = "personal use for nexus dashboard"
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = [format("%s/%s", data.external.whatismyip.result["internet_ip"], 32)]
  }


  ingress {
    description = "personal use for nexus"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [format("%s/%s", data.external.whatismyip.result["internet_ip"], 32)]
  }

  ingress {
    description     = "usage from jenkins, upload artifacts"
    from_port       = 8081
    to_port         = 8081
    protocol        = "tcp"
    security_groups = [aws_security_group.jenkins_sg.id]
  }

    ingress {
    description     = "usage from app, download artifacts"
    from_port       = 8081
    to_port         = 8081
    protocol        = "tcp"
    security_groups = [aws_security_group.app_sg.id]
  }


  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_security_group" "sonar_sg" {
  name        = "sonar-sg"
  description = "sonar-sg"

  ingress {
    description = "personal use of sonarqube dashboard"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [format("%s/%s", data.external.whatismyip.result["internet_ip"], 32)]
  }

  ingress {
    description = "personal use for sonarqube"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [format("%s/%s", data.external.whatismyip.result["internet_ip"], 32)]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

# this rule depends on both security groups so separating it allows it
# to be created after both
resource "aws_security_group_rule" "jenkins_sg_and_sonar_sg_cycle_communication" {
  security_group_id        = aws_security_group.jenkins_sg.id
  from_port                = 0
  to_port                  = 65534
  protocol                 = "tcp"
  type                     = "ingress"
  source_security_group_id = aws_security_group.sonar_sg.id
}

resource "aws_security_group_rule" "sonar_sg_and_jenkins_sg_cycle_communication" {
  security_group_id        = aws_security_group.sonar_sg.id
  from_port                = 0
  to_port                  = 65534
  protocol                 = "tcp"
  type                     = "ingress"
  source_security_group_id = aws_security_group.jenkins_sg.id 
}



